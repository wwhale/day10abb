(function () {
    "use strict";
    angular.module("RegApp").controller("RegCtrl", RegCtrl);
    
    RegCtrl.$inject = ["$http"];

    function RegCtrl($http) {
        var regCtrlself  = this;
        
        regCtrlself.onSubmit = onSubmit;
        regCtrlself.initForm = initForm;
        regCtrlself.onReset = onReset;
        regCtrlself.onSubmitshow = onSubmitshow;
        regCtrlself.minAge=minAge;
        regCtrlself.checkAge=checkAge;
        


        regCtrlself.emailFormat = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
        regCtrlself.user = {
            
        }

        
        regCtrlself.nationalities = [
            { name: "Please select" , value:0},
            { name: "Singapore", value: 1},
            { name: "Indonesia", value: 2},
            { name: "Thailand", value: 3},
            { name: "Malaysia", value: 4},
            { name: "Australia", value: 5}      
        ];


       

        function minAge() {
 
            if(regCtrlself.user.dob != undefined ){
            regCtrlself.userAge = checkAge(regCtrlself.user.dob);
            //console.log("DOB > " + regCtrlself.user.dob);
            //console.log("userAge > " + regCtrlself.userAge);
            //console.log("userAgeOK > " + regCtrlself.userAgeOK);
            
            regCtrlself.userAgeOK = !(regCtrlself.userAge>=18);
            //console.log("userAgeOK > " + regCtrlself.userAgeOK);


            // to set the $error.required
            if (regCtrlself.userAgeOK == true){
                regCtrlself.registrationform.dob.$error.required = true;
            } else {
                regCtrlself.registrationform.dob.$error.required = false;
            }
            
            return regCtrlself.userAgeOK;
            }
        }

        function checkAge(date) {
            var checkdob = new Date(date);
            var ageDifMs = Date.now() - checkdob.getTime();
            var ageDate = new Date(ageDifMs); // miliseconds from epoch
            return Math.abs(ageDate.getUTCFullYear() - 1970);
        }


        function onSubmitshow(){

            if (regCtrlself.submitOKShow == 1)
                return true;
        }


        function initForm(){
            regCtrlself.user.selectedNationality = "0";
            regCtrlself.user.gender = "M";
            regCtrlself.submitOKShow = 0;
            regCtrlself.userAge = 0;
            regCtrlself.userAgeOK = false;
        }

        function onReset(){
            regCtrlself.initForm(); 
            regCtrlself.user = Object.assign({}, regCtrlself.user);
            regCtrlself.registrationform.$setPristine();
            regCtrlself.registrationform.$setUntouched();
        }

        function onSubmit(){
            console.log("--capture data--")
            console.log(regCtrlself.user.email);
            console.log(regCtrlself.user.password);
            console.log(regCtrlself.user.confirmpassword);
            console.log(regCtrlself.user.gender);
            console.log(regCtrlself.user.fullName);
            console.log(regCtrlself.user.dob);


            console.log(regCtrlself.user.address);
            console.log(regCtrlself.user.selectedNationality);
            console.log(regCtrlself.user.contactNumber);

            $http.post("/api/register", regCtrlself.user).then((result)=>{
                console.log("result > " + result);
                regCtrlself.user = result.data;
                console.log("--Check data receive by backend--")
                console.log("Email > " + regCtrlself.user.email);
                console.log("password > " + regCtrlself.user.password);
                console.log("confirmpassword > " + regCtrlself.user.confirmpassword);
                console.log("fullName > " + regCtrlself.user.fullName);
                console.log("gender > " + regCtrlself.user.gender);
                console.log("Date of birth > " + regCtrlself.user.dob);
                console.log("fullName > " + regCtrlself.user.address);
                console.log("selectedCountry > " + regCtrlself.user.selectedNationality);
                console.log("contactNumber > " + regCtrlself.user.contactNumber);

                console.log("submitOKShow > " + regCtrlself.submitOKShow);
                regCtrlself.submitOKShow = 1;
                console.log("submitOKShow > " + regCtrlself.submitOKShow);

            }).catch((error)=>{
                console.log("error > " + error);
            })
        }

       

        regCtrlself.initForm();
    }
    
})();    